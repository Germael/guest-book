from flask import Flask
from flask_restful import Resource, Api, reqparse

app = Flask(__name__)
api = Api(app)
parser = reqparse.RequestParser()
parser.add_argument('message')
parser.add_argument('customer')

book_data = dict()


class GuestBook(Resource):

    def get(self):
        return book_data

    def post(self):
        args = parser.parse_args()
        if args.get('message') and args.get('customer'):
            book_data[args['customer']] = args['message']
        return 'data added'


api.add_resource(GuestBook, '/')

if __name__ == '__main__':
    app.run(debug=True, port=8080, host='0.0.0.0')
