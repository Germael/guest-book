FROM ubuntu:latest
COPY . .
RUN apt-get update && apt-get upgrade -y
RUN apt-get install python3 -y && apt-get install python3-pip -y
RUN pip3 install flask && pip3 install flask_restful
EXPOSE 8080
CMD ["python3", "main.py"]